CC= gcc
CFLAGS+= -O0 -ggdb3
CFLAGS+= -I..
LDFLAGS+= $(LDADD)
OBJS= $(SRCS:.c=.o)

all: $(PROG)

$(PROG): $(OBJS)
	$(PRELINK)
	$(CC) -o $@ $^ $(LDFLAGS)
	$(POSTLINK)

clean:
	$(RM) -f $(OBJS) $(PROG)

install: $(PROG)
	echo "install $(PROG) /to/path"
