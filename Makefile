SUBDIRS= zfs-tests

all install clean:
	@for d in $(SUBDIRS); do\
		$(MAKE) -C $$d $@ || exit $$?;\
	done
